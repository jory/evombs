
#include <stdio.h>
#include <stdlib.h>

#include <vector>
#include <map>
#include <math.h>
#include <time.h>
#include <iostream>
#include "globalConst.h"
#include "tHMM.h"
#include "tAgent.h"
#include "tGame.h"
#include <string.h>

#ifdef _WIN32
#include <process.h>
#else
#include <unistd.h>
#endif

#define randDouble ((double)rand()/(double)RAND_MAX)

using namespace std;

//double replacementRate=0.1;
double perSiteMutationRate=0.005;
int update=0; // generation counter
int repeats=4; // # times to evaluate and gather fitness each individual (not necessary for deterministic)
int maxAgent=100; // population size
int totalGenerations=1000;

int main(int argc, char *argv[])
{
	vector<tAgent*>agent;
	vector<tAgent*>nextGen; // write-buffer for population
	tAgent *masterAgent; // used to clone and start LoD
	int i,j;
	tGame *game;
	double maxFitness;
	FILE *genomeFile;
	FILE *LOD;
	LOD=fopen(argv[1],"w+t");
	genomeFile=fopen(argv[2],"w+t");	

	/// setup initial population
	srand(getpid()); // need different includes for windows XPLATFORM
	agent.resize(maxAgent);
	game=new tGame;
	masterAgent=new tAgent; // can also loadAgent(specs...)
	masterAgent->setupRandomAgent(5000); // create with 5000 genes
	masterAgent->determinePhenotype();
	for(i=0;i<agent.size();i++){
		agent[i]=new tAgent;
		agent[i]->inherit(masterAgent,0.01,0); // {}(*from, mu, t);
	}
	nextGen.resize(agent.size());
	masterAgent->nrPointingAtMe--; // effectively kill first ancestor from whom all inherit

    ///the main loop
	while(update<totalGenerations){
		for(i=0;i<agent.size();i++){ // reset: fitness, fitnesses
			agent[i]->fitness=0.0;
			agent[i]->fitnesses.clear();
		}
		for(i=0;i<agent.size();i++){
			for(j=0;j<repeats;j++){
				game->executeGame(agent[i]);
				agent[i]->fitnesses.push_back((float)agent[i]->fitness);
			}
		}
        // find maximum fitness
		maxFitness=0.0;
		
		for(i=0;i<agent.size();i++){
            //summ up all fintesses in the finessess buffer
            agent[i]->fitness=0.0;
			for(j=0;j<repeats;j++)
                agent[i]->fitness+=agent[i]->fitnesses[j];
            // normalize by fitness
            agent[i]->fitness/=(double)repeats;
            
			if(agent[i]->fitness>maxFitness){
				maxFitness=agent[i]->fitness;
            }
		}
		cout<<update<<" "<<(double)maxFitness<<endl;
        //roulette wheel selection
		for(i=0;i<agent.size();i++){
			tAgent *d;
			d=new tAgent;
			do{
                j=rand()%(int)agent.size();
            } while(randDouble>(agent[j]->fitness/maxFitness));
            
			d->inherit(agent[j],perSiteMutationRate,update);
			nextGen[i]=d;
		}
        // moves "nextGen" to current population
		for(i=0;i<agent.size();i++){
			agent[i]->retire();
			agent[i]->nrPointingAtMe--;
			if(agent[i]->nrPointingAtMe==0)
				delete agent[i];
			agent[i]=nextGen[i];
		}
		agent=nextGen;
		update++;
	}
	agent[0]->ancestor->ancestor->saveLOD(LOD,genomeFile);
    agent[0]->ancestor->ancestor->determinePhenotype(); // transcribe genetic material
    agent[0]->ancestor->ancestor->showPhenotype();
    return 0;
}

