#!/bin/bash

GPU_PARAM=$1

#module unload GNU
#module try-load PGI/13.10
err=
if [ "$GPU_PARAM" == "gpu" ]; then
	pgcpp -acc -ta=nvidia -Minfo=accel -Minform=severe -o evomb main.cpp tAgent.cpp tGame.cpp tHMM.cpp
	GPU_PARAM=
else
	#pgcpp -Minform=severe -o main main.cpp tAgent.cpp tGame.cpp tHMM.cpp
	err=$(g++ -o evomb main.cpp globalConst.h tAgent.* tGame.* tHMM.*)
fi

if [ $? -eq 0 ]; then
	echo "to run: ./evomb lod.txt genome.txt"
fi
